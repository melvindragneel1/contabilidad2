/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Mysql.MySQL;
import Vista.Menuingresa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class IngresaController {
    Connection conn;
    Menuingresa interfaz;
    int item;
    public IngresaController(Menuingresa GUI){
    interfaz=GUI;
    conn= MySQL.getConexion();
    }
    
public void llenacombo(){
 try{
            
            String sql ="select nombre from cuentas;";
            PreparedStatement pstm = conn.prepareStatement(sql);
        
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
                interfaz.cuenta.addItem(rs.getString(1)); 
            }
        }catch (Exception e) {
            //System.out.println(e.getMessage());
        }    
}

public void insertar()
    throws SQLException {
    obtenerId();

        String sql
                = "insert into cuentas.movimientos values (?,?,?,?,?,?)";
        PreparedStatement pst = null;
        pst = conn.prepareStatement(sql);
        pst.setInt(1, Integer.parseInt(interfaz.t1.getText()));
        pst.setString(2, interfaz.t2.getText());
        pst.setInt(3, Integer.parseInt(interfaz.t3.getText()));
        pst.setDate(4, java.sql.Date.valueOf(interfaz.t4.getText()));
        pst.setInt(5, item);
        pst.setString(6, interfaz.t5.getSelectedItem().toString());
        int n = pst.executeUpdate();
        JOptionPane.showMessageDialog(null, "MOVIMIENTO AGREGADO");
        System.err.println(
                n);
    }

public void obtenerId() throws SQLException{
                
            String sql ="select id from cuentas where nombre=?;";
            
            PreparedStatement pstm = conn.prepareStatement(sql);
            pstm.setString(1, interfaz.cuenta.getSelectedItem().toString() );
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
               item=Integer.parseInt(rs.getString(1)); 
            }
        
    
}


}
